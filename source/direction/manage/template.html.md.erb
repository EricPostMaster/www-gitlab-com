---
layout: markdown_page
title: Product Stage Direction - Manage
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Stage Overview
 
The Manage stage in GitLab **lays a foundation for organizations to scale and work more efficiently** in GitLab. Managing a piece of software is more than maintaining infrastructure; tools like GitLab need to be adoptable by companies of all sizes and be easy to operate. Setting up your processes shouldn’t be a struggle, and administrators shouldn’t have to compromise on security or compliance to prevent tools from hindering their velocity.

* **Laying the foundation**: to make GitLab adoptable by organizations of any size, it must excel at meeting table stakes. A skyscraper with many people in it can only be enabled by a solid, secure foundation - an application serving a similar scale isn't any different. GitLab needs to support the access control, onboarding, security, and auditing needs that enables enterprise-level scale. We also need to make the foundation easy to lay; constructing a building is slow and arduous when it's done brick-by-brick. Adopting GitLab should be fast and reliable, getting an organization off the ground effortlessly.
 
* **Working more efficiently**: while we want to fulfill foundational needs, GitLab strives to give you the ability to work in new and powerful ways. We aspire to answer valuable questions for users and to automate away the mundane. It’s not enough to give instances the ability to meet their most basic needs; as a single application for the DevOps lifecycle, GitLab can exceed the standard and enable you to work in ways you previously couldn’t.

### Groups in Manage

Manage is composed of 4 distinct groups that support our mission of **laying a foundation for organizations to scale and work more efficiently** in GitLab:
 
_Laying the Foundation_
 
* Access: leads on our foundation of configuration and access control that enables secure and enterprise-level scale.
* Import: leads on adoption, making it easy for organizations of all sizes to start flying in GitLab quickly and with ease.
 
_Working More Efficiently_
 
* Compliance: leads on finding more efficient, enjoyable ways for organizations to stay compliant in GitLab - and prove it.
* Analytics: leads on helping users understand the end-to-end process of how they work, where bottlenecks and waste are, and how they can find efficiencies to work more quickly and eliminate risk.

## 3 Year Strategy

TBD

## Plan for 2020

In 2020, the Manage stage will provide such compelling, must-have value to large customers that we will be able to **attribute over $100M in ARR to Manage capabilities by end of year**. This means that Manage is a must-have part of the feature set that supports that customer, or Manage was a key part of their adoption journey.
 
**3 themes will guide Manage in 2020**:

### Enterprise readiness
We're going to focus on increasing and retaining the number of customers with enterprise-grade needs by solving their most compelling problems. We're doing this by focusing on:
* Enterprise-grade authentication and authorization.
* Great auditability.
* Isolation and control, especially for GitLab.com.
* Improving tools that help compliance-minded organizations thrive.
 
Success in this theme looks like:
* Increased adoption by self-managed and GitLab.com organizations over X members?
* Increased NPS by organizations over X members?
 
You can track progress against this theme [here](https://gitlab.com/groups/gitlab-org/-/epics/3242).

### Value in GitLab Ultimate
We're going to drive an Ultimate story that creates obvious, compelling value. Currently, the majority of Ultimate's value lies in application security. We will strive to improve the breadth of this tier's value proposition by driving more value in Ultimate, such as:
* Driving compliance efficiencies.
* Powerful analytical insights.
 
Success in this theme looks like:
* Increased % of IACV in Ultimate/Gold
* Increased adoption and engagement with Ultimate-level features
 
You can track progress against this theme [here](https://gitlab.com/groups/gitlab-org/-/epics/3243).

### Easy adoption

Manage will create easy paths to support our land-and-expand strategy. There's a starting point for any organization with an expansive new tool, and Manage will make this transition easy by supporting natural starting points - ideally in Core, for all groups - that get our customers started and hooked on GitLab:
* Frictionless import at any scale
* Drive entry-level enterprise table stakes into Core
* Authentication/group management ease of setup
 
You can track progress against this theme [here](https://gitlab.com/groups/gitlab-org/-/epics/3244).

## How we operate
Manage operates under GitLab's values, but is a stage that seeks to particularly excel in certain areas that support our goals above. We seek to be leaders at GitLab by:

### Focusing in
* Depth over breadth. For the most part, we’re biased toward doubling down and investing on what’s working rather than extending the breadth of our stage. 
* We prioritize aggressively against the themes above, deliberately deciding to not pursue initiatives that don’t support our 2020 goals.
* We have a core experience loop for each group, and focus on making it lovable.
* We lead the field in our level of customer focus. We work extensively on dogfooding with internal customers, and validate the improvements we make with users outside of GitLab.

### Always be shipping
* We lead the way on iteration, prioritizing valuable MVCs that support our 2020 goals and make the product incrementally better.
* Our iteration is supported by a great planning and development process, that gives us checkpoints to keep issues small and incremental. As a result, Manage’s throughput is high. 

### Measuring what matters
* Great instrumentation through North Star dashboards
* Measuring business value by tying customers and ARR to Manage capabilities 

### Great team
* Best team at GitLab with high individual satisfaction
* Great work-life balance

You can track our operational goals [here](https://gitlab.com/groups/gitlab-org/-/epics/3245).

## Categories

<%= partial("direction/categories", :locals => { :stageKey => "manage" }) %>

## Upcoming Releases

<%= direction["all"]["all"] %>

<%= partial("direction/other", :locals => { :stage => "manage" }) %>
