---
layout: handbook-page-toc
title: Modern Health
description: Information on GitLab's Employee Assistance Program.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## Employee Assistance Program

GitLab offers an Employee Assistance Program to all team members via [Modern Health](https://www.joinmodernhealth.com/). Modern Health provides technology and professional support to help reduce stress, feel more engaged, and be happier. Through GitLab, you have access to coaching sessions at no cost to you.

### What does Modern Health offer?

Modern Health is the one-stop shop for all tools related to mental well-being and self-improvement. Members gain access to the following features:
* **Personalized Plan:** Take a well-being assessment and review which tools may be most helpful for you.
* **Professional Support** Get matched to a dedicated coach or therapist who can help you reach your personal and professional goals. Coaching visits are covered by GitLab and are offered at no cost to you.
* **Curated Content Library:** Learn quick tips and tricks to prevent burnout, manage stress, and cope with anxiety or depression based on our evidence-based digital programs. You can use these digital programs for in-the-moment support or to build your own toolkit of techniques to improve your mental well-being.

If you’re still not sure where to get started, we recommend that you:
1. Download the app and register your account
2. Register and take the well-being assessment and
1. Get matched to a dedicated coach who can work with you to figure out next steps.

### Which areas does Modern Health support?

Modern Health cultivates the resilience needed to weather the ups and downs of everyday life. Here are the specific areas where we can help:
* Work Performance: Productivity, Leadership Skills, Work Relationships, Professional Development
* Relationships: Romantic Relationships & Dating, Family, Friends, Breakups
* Stress & Anxiety: Anxiety, Depression, Stress, Resilience
* Healthy Lifestyles: Sleep, Physical Activity, Eating Well, Habits
* Financial Well-being: Goals, Budgeting Savings and Debt, Management, Investing
* Diversity & Inclusion: Gender, Equality, Unconscious Bias, LGBTQ
* Life Challenges: Pregnancy/Parenting, Elder/Child Care, Loss of a Loved One, Illness
* Mindfulness & Meditation: Stress Less, Sleep Better, Focus Better, Meditation for Beginners

Note: This list isn’t intended to be comprehensive. Please reach out to `help@joinmodernhealth.com` with any questions about how or where to get started.

### How does Modern Health think about mental health?

The philosophy towards mental health comes from the World Health Organization (WHO): “in which every individual realizes his or her own potential, can cope with the normal stresses of life, can work productively and fruitfully, and is able to make a contribution to her or his community.”

### When do my Modern Health benefits reset?

Your benefits reset 1 year from the launch date, on May 15th, 2020.

### What languages is Modern Health available in?

The platform is currently available in English and Spanish. If English or Spanish is not your preferred language, please note Care is provided in most languages. Feel free to email `help@joinmodernhealth.com` to find out more.

### Registration

* Am I eligible? All GitLab team members are eligible for Modern Health.
* How do I register?
  * Download the Modern Health app in the [Google Play Store (Android)](https://play.google.com/store/apps/details?id=com.modernhealth.modernhealth) or [App Store (iOS)](https://itunes.apple.com/us/app/modern-health/id1445843859?ls=1&mt=8).
  * After your download is complete, select “Join Now” from the welcome page of the mobile app.  
  * Use the first and last name you have on file with GitLab.
  * Verify using your company email.
  * Enter the your GitLab email and password of your choice. (Reminder that you must register with your GitLab email, but you can change your Modern Health account email to your personal email in Settings upon registration.)
  * Select “Register” on the web or “Agree & Join” on the mobile app to complete registration.

If you have trouble registering for Modern Health, please don’t hesitate to reach out to `help@joinmodernhealth.com` with a note or screenshot. Their customer support team will verify the information against what they have on file to provide you the best instructions on how to successfully access Modern Health.

### What is the well-being survey, and why should I take it?

Similar to an annual physical with your primary care physician, Modern Health’s well-being survey serves as a check up for your mental health. You can retake the survey to track your progress overtime.

Your well-being score empowers experts at Modern Health to provide you the best user experience. It enhances the customization of your personalized wellness plan, which makes it more effective in addressing your specific needs. Although the ups and downs in well-being score are inevitable, the data-driven approach keeps up with how you’re doing over time to support you with the tools to improve no matter where your score is at today.  

### Care

**What is coaching?**
Coaching is a collaborative process to help you make important changes in your personal and professional life. Your coach is there to help you figure out how you want to change and the steps you need to take to do so. Your coach’s job is to help you organize your thoughts, emotions, and goals and break things down into smaller steps that create forward movement and growth. The client is the driver of these sessions, while the coach is there to provide reflection, clarity, and accountability.

**What is the difference between coaching and therapy?**
Modern Health’s belief is that anyone can benefit from working with a coach, and some people need therapy in addition to or instead of coaching. The primary difference between coaching and therapy is that therapy is conducted by licensed mental health professionals who are trained to treat clinical difficulties (e.g., depression, anxiety) whereas coaches work on non-clinical issues (e.g, personal growth, financial well-being, and professional development). If you are experiencing a clinical need, please access therapy through your health insurance or EAP. You can also work with your dedicated coach to determine if you would benefit from therapy.

**How do you match me to a coach?**
Modern Health matches you to appropriate coach based on a proprietary algorithm that takes into account your well-being assessment and areas you want to focus on.

**How often should I meet with my coach?**
How often you meet with your coach depends on your personal situation. Some people like to meet weekly, whereas others meet every month or two.

**Can I complete sessions with my partner or a family member?**
Yes, you are able to use your sessions with your partner or a family member. If you choose to do so, each completed session will count as one of your covered sessions.  Please discuss this directly with your coach.

**What can I expect during my first coaching session?**
During the first session, your coach will kick things off with an introduction to what coaching is and what you can expect during the session, including reviewing confidentiality (nothing you discuss is shared with Modern Health or your employer unless you request more support or need crisis resources). They will then ask you a few questions to better understand what you hope to get out of the coaching experience and what a successful experience would be for you. By setting expectations up front, the coach will be able to better help you identify personal goals and take steps towards achieving them. In subsequent sessions, the coach will follow up on any action items to understand their impact and you will together come up with next steps. The standard coaching session is 30 minutes and takes place over video, usually every two weeks or once a month.  In between sessions, you are encouraged to reach out to your coach with any follow up questions or advice via Modern Health’s digital messaging tool in the app as well as utilize the digital CBT and meditation programs.

**How should I prepare for my first session?**
Before your first session, we encourage you to ask yourself a few questions. As with many other aspects of your life, the more effort you put into your coaching experience, the more you’ll get out of it. These can help guide your initial conversations with your coach.
* Where do you want to start?
* You might have a lot of areas of interest (e.g., you want to learn mindfulness, and work on your finances). Spend some time thinking about where you want to get started before your first visit. What is most on your mind or stressing you out?
* Do you know what your goals are or do you need help figuring that out?
* What do you want to get out of coaching?
* What do you want your coach to be like? Do you want someone who holds you accountable, who is a cheerleader, who challenges you?
* How do you want your life to look when you are done? What does success look like?

**What happens if my provider isn’t a good fit?**
Our goal at Modern Health is to find someone that you feel you can do good work with and who can do good work with you! If you think the first person you meet with doesn’t seem like a good fit, just let the Modern Health team know (help@joinmodernhealth.com) and they will connect you with someone new.

### Logistics

* You have unlimited access to your coach over text and/or email.
* Coaching sessions are held over video/phone.
* If you need to miss a scheduled session please let your coach know at least 24 hours before your appointment. If you cancel after that time, or miss the session, it will count towards your total covered sessions.

**What do I do if I need support in between sessions?**
Beyond sessions, your coach is available through chat to check in on your progress toward your goals, as well as provide ad hoc support when situations come up that you might need advice on, but don’t need a full session for. To chat with your coach, click the blue text bubble on the bottom center of the mobile app.

Note: Modern Health is not a crisis resource. If you are experiencing a mental health emergency, please go to the nearest emergency room or contact a local emergency response line. You can find local and international resources by selecting “Information” on the bottom right of your mobile app, and then clicking the red “Access to 24/7 Crisis Information” banner at the top of the screen.

### Confidentiality

**Does my employer know if I’m using Modern Health?**
All information submitted through the Modern Health application is kept confidential and used to deliver a more personalized experience. No individual usage data will ever be shared back with your employer.

**Is what I discuss with my coach confidential?**
All information between you and your coach is confidential, except in the following cases:
* You are at risk of harming themselves and/or others
* Child, elder adult, or dependent adult abuse
* Court subpoenas

**How do you protect my information?**
Hypertext Transfer Protocol Secure (HTTPS) encryption measures for all data exchanged between our members and the application. Conversations are encrypted in transit via SSL (TLS v1.2) and each conversation (between a member and a provider) has its own encryption key which is stored in a separate, secure management system. Message contents are encrypted upon receipt by the web server and are transported and stored in internal systems.

### Crisis Support

**What do I do in a crisis?**
If you are experiencing a crisis (e.g., thoughts about suicide, thoughts about harming yourself or others, medical crisis, or in a dangerous situation) please call:
* emergency responders (911 or the [appropriate local emergency number for your area](https://en.wikipedia.org/wiki/List_of_emergency_telephone_numbers))
* in the US/Canada, https://suicidepreventionlifeline.org/ or 1-800-273-8255) or [a crisis line in your area](https://en.wikipedia.org/wiki/List_of_suicide_crisis_lines)
* head to the nearest emergency room.

### Modern Health Weekly Reporting

* Pull the Modern Health BambooHR report
* Sort by hire date, remove anyone who hasn't started and copy the report
* Open the `Modern Health Upload` spreadsheet on the google drive and paste the report under a new tab
* Rename the tab to today's date
* Download as a CSV file
* Email the file the secure upload using Box
