---
layout: handbook-page-toc
title: "Using Gainsight within Customer Success"
---

# Using Gainsight within Customer Success
{:.no_toc}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What is Gainsight?

Gainsight is a tool for Technical Account Managers to manage the ongoing customer lifecycle. 

### Key Benefits of Gainsight

Gainsight will help across several different areas within Customer Success. Some highlights include: 
* Efficiency: consolidated account views (BoB, account), telemetry, Zendesk integration
* Consistency: Establish customer lifecycle process, manage and track engagement
* Visibility: health scores, risk, adoption
* Automation: process, adoption, enablement for digital journey 
* Metrics and Analytics: Stage Adoption, customer health, time-to-value, 
* Grow Net Retention: Success plan-driven engagement, expand plays


### How to Get Started

1. Login to SalesForce, and click on the Gainsight NXT tab at the top of the screen. 
   1. If you don't see Gainsight NXT as a choice, you can quickly add it by clicking the "+" sign, choosing "Customize My Tabs" and choosing Gainsight NXT from the applications list. 

Note, you may log in to Okta but you will not have subscription data, opportunity, or SalesForce activity.

## Key Features

### Gainsight 360

Add text

### Logging Activities in Timeline

Add text

### Building a Success Plan

Learn more about how to build a [Success Plan in Gainsight](/handbook/customer-success/tam/success-plans/)
