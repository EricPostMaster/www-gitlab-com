---
layout: handbook-page-toc
title: "Account Based Marketing"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What is account based marketing?
Account-based marketing is a strategic approach to marketing based on account awareness in which an organization considers and communicates with individual prospect or customer accounts as markets of one.  Through a close alignment between sales and marketing we focus on target accounts that fit our ICP or ideal customer profile.  At GitLab, we are at the beginning of our account based marketing efforts and in the process of defining our ICP and aligning our target accounts based on those criteria.

## Where does account based marketing fit within the greater marketing org?
Account based marketing sits next to field marketing and is similarly aligned to sales.  Account based marketing looks at an account or target account as a market of one, versus marketing to the total addressable market or region.  This type of marketing is executed by the account based team where as field marketing etc is focused on lead gen and account centric marketing.

## What does account based marketing look like at GitLab and what is the team responsible for?
The account based marketing team is responsible for all account based marketing strategies, finding and targeting our ideal customer, and managing our ideal customer profile.  You can think of this team as sitting next to all other marketing functions and marketing to our highest value target accounts utilizing and building on the other marketing efforts already in motion.

**Example:** Once an ABM target account in the NORAM West region is identified, the ABM team will work with the SAL, SDR and SA/TAM, plus the FMM and other stakeholders for the account to develop a marketing plan for that account that encompasses events and other field marketing tactics in the region, a digital campaign relevant to the company's goals and painpoints, and other avenues including but not limited to direct mail, marketing drip campaigns, social media etc.

## Roles & Responsibilities

**Emily Luehrs**  
*Account Based Marketing Manager*
* **Development**: plan account based marketing strategy, prioritize company objectives as it aligns with ABM
* **Strategy**: plan, prioritize and manage execution of campaigns
* **Ideal Customer Profile**: acts as project manager for the development of our ICP

**Jenny Tiemann**  
*Sr. Marketing Program Manager*
* **Campaigns**: organize execution, timeline, and campaign tracking

**Leslie Stinson**  
*Digital Marketing Manager*
* **Campaigns**: develop and manage digital assets and implementation for ABM campaigns
 
## SDR<>ABM Alignment
As the account based team is focused on our high value accounts, it is natural to pair our acceleration team with the account based team to help accelerate the overall opportunity.  Beginning Q2 FY21 we will be pairing the acceleration team and account-based team in order to align our most senior SDR’s to our highest value accounts while also solving for the need to accelerate sales onboarding and warming up sales territories.  In parallel, we will also be incorporating the mapped SDR team in region into the plan and motions of the acceleration teams so as to encourage the transition to more acceleration based tactics across the entire organization, utilizing the intent data and other tools through the account based team.

### Alignment

| Account Based Marketing | Acceleration | Geo (Regional) Acceleration |
| ------ | ------ | ------ |
| * developing territory target lists <br> * account based strategy & campaigns <br> * ideal customer profile <br> * help sales answer where we should land next? <br> digital coverage for acceleration + new territory <br> * help drive `other tactics` for field marketing  | * account based marketing SDR functions <br> * warming up new regions/territories <br> * outbound innovation <br> * MQL generation <br> * 200 top intent account per month <br> * targeted net new outbound | * inbound lead qualification <br> * drift qualification <br> * add-on/upsell outbound <br> * targeted net new outbound (max of 50 accounts) <br> * mql generation <br> * field marketing coverage |


### What does this look like for the SDR organization?

| Team | Q2 FY21 | Q3 FY21 | Q4 FY21 |
| ------ | ------ |------ |------ |
| Account Based Marketing | (in process already) Roll out intent data to acceleration for territory planning <br> <br> Begin serving digital “warm up” through Demandbase 30 days out from accel team landing | Roll out next iteration of the ICP  <br> <br> regional intent dashboards for SDR org |  |
| Acceleration Team |  Align with ABM team to target accounts in our ABM strategy <br> <br> Land in existing territories with geo rep for territory planning and acceleration training | AMER - 800 accounts being worked per campaign (60 day period over 4 sales territories) <br> <br> EMEA - 500 accounts being worked per campaign (60 day period over 8 sales territories) |  |
| Regional SDR | Meeting and IACV goals implemented <br> <br> begin working with acceleration team on accel motion in territory | AMER - 800 accounts being worked per campaign (60 day period over 4 sales territories) <br> <br> EMEA - 500 accounts being worked per campaign (60 day period over 8 sales territories) | Enablement to acceleration function completed by end of Q4 | 


## Tools we use

#### Demandbase 
Targeting and personalization platform which we use to target online ads to companies that fit our ICP and tiered account criteria.  Demandbase also has a wealth of intent data that is available to us through it's integration with Salesforce.  You can learn more about this in the Intent Data section below.

**Demandbase Audiences**
We use the following naming convention to identify audience lists in Demandbase:

*  (AB) Account Based Marketing
*  (AC) Acceleration team
*  (FM) Field Marketing
*  (CM) Corporate Marketing
*  (List) General label used to identify if a list is being pulled in by another system i.e. Salesforce

#### TOPO
Research and advisory firm used by companies to develop and orchestrate their account based strategy.  We will be following their model for developing our ideal customer profile (ICP) and account based orchestration plays.
[TOPO research we are using](https://drive.google.com/drive/folders/1PC9Fqri-_JiJM1107B7k-ejD20gV3CnM?usp=sharing)

## Account Based Marketing workflow & labels in GitLab   

The ABM team works from issues and issue boards. If you are needing our assistance with any project, please open an issue and use the ~Account Based Marketing label anywhere within the GitLab repo. 

The ABM team uses this [global issue board](https://gitlab.com/groups/gitlab-com/-/boards/1409957) and also has the [Account Based Marketing Project](gitlab.com/gitlab-com/marketing/account-based-marketing)

Global labels used by the team:  
- `Account Based Marketing`: pulls the issue into the board, and is used to put an issue on the team's radar
- `ABM FYI`: used to put something on the account based marketing team's radar, however they are not the DRI not is any action needed at this time (example: and account centric campaign being run by field marketing)
- `ABM Campaign`: Used to identify account based marketing campaigns
- `ABM::plan`: work or campaign that has been proposed and is awaiting evaluation form the team.  This includes additional being added to our ABM strategy and campaign proposals
- `ABM::design`: work or campaign that has been accepted by the ABM team and is being flushed out before starting the execution work
- `ABM::wip`: issues that are currently being worked on by the ABM team
- `ABM::blocked`: issues/work that is currently blocked.  The team may be waiting in additional information or could be planning to execute but at a later date due to other circumstances that will be noted in the issue
- `ABM tier 1`: issues and epics related to tier 1 campaigns
- `ABM tier 2`: issues and epics related to tier 2 campaigns
- `ABM tier 3`: issues and epics related to tier 3 campaigns
- `ABM Nomination`: tracks all abm account nominations
- (deprecated) `ABM 30 day`, `ABM 60 day`, & `ABM 90 day`: labels used to track our initial rollout of our ABM program.  These labels are used by the internal team only, but our board is organized so that other can follow along

## Ideal Customer Profile
An ideal customer profile is the description of our "perfect" customer company (not individual or end user).  The profile takes into consideration firmographic, environmental and additional factors to develop our target list of highest value accounts.  The account based marketing team is responsible for the management of the ICP and reviews of our ICP as it iterates and changes based on time, company goals, and product maturity.

Gitlab recently developed our ideal customer profile in FY21 Q1 to help focus our efforts on the highest value accounts.  Below is the profile we set.  One thing to note about our ICP is that it is fairly broad, mainly because GitLab can ultimately sell to a vast number of companies versus say, a banking solution that would have a much smaller TAM (total addressable market).  Because of this, we do not target ALL the accounts that fit our ICP at a given time, but rather, focus on a subset that we can execute on given resources and based on feedback from sales and marketing.

|  | **Attribute** | **Description** |
| ------ | ------ | ------ |
| **Core criteria (must haves)** | Number of developers (using company size as proxy) | 2,000+ |
| | Tech stack | Includes GitHub, Perforce, Jenkins, BitBucket or Cloudbees |
| | Titles that exist within the company | Includes Application development manager/director/executive & CISO  |
| | Prospect/Customer | Not a current PAID customer for GitLab  |
| **Additional criteria (attributes to further define)** | Annual technology spend | 4% of annual revenue | 
| | High intent account | Account is trending as high intent based on our data in Demandbase |
| | New hire | VP of IT, DevOps leadership role |

### Tier 1 (1:1 or strategic) - ICP accounts
**Our highest value target accounts.  Will include 5-10 accounts at any given time globally. Will receive customized account marketing plan w personalized content**
1.  be a 100% match to our ideal customer profile + additional criteria based on our focus at the time (i.e. a certain vertical, etc)
1.  be showing `HIGH` intent signals
1.  top account for sales
1.  have an engaged sales team ready to enact a fully customized marketing plan
      - have a completed marketing plan developed through collaboration of the ABM and Sales team for that account

### Tier 2 (1:few or scale)-Like accounts based on a certain number of target account qualifiers
**Will include roughly 50 accounts at any given time globally.  These accounts will receive an orchestration play that is a medium lift and focused on like accounts.  This mean there may be three different campaigns running in the segment, focused on a set of accounts rather than customized to a single account.**
1.  account fitting our ideal customer profile (ICP)
1.  must be showing `HIGH` intent (based on Demandbase data)
1.  high priority for sales but may have a lower lifetime value for GitLab
1.  falls into the banking, financial services, and transportation verticals

### Tier 3 (1:many or programmatic)- Accounts we would like to target but without the personalize plan or resources
**Will include 75-100 accounts globally at any given time. These accounts will have a digital campaign based on use case along with light email and SDR support**
1.  fits our ICP criteria AND
1.  showing `HIGH` intent signals in Demandbase OR
1.  is a focus for sales in the territory

## When does an account get added to our account based marketing strategy or move tiers?
The account based marketing team will be monitoring accounts in all tiers and adjusting the level of marketing support for these accounts based on the accounts tiering qualifications.  For an example, if an account is currently in tier 3 and then meets all of tier 2 qualifications, that account will be moved to tier 2.  Additionally, the account team will be notified and brought in to strategize and engage in the marketing plan appropriately.

Accounts can also move tiers by being nominated by sales and accepted by the account based team.  This is done through completing this [issue template](https://gitlab.com/gitlab-com/marketing/account-based-marketing/-/issues/new#) and assigning it to @emilyluehrs.  The account will be evaluated by the account based team which has an SLA for a decision within 5 business days of the issue being submitted.

## Other times when accounts will move
As our ICP iterates (we will be reviewing in July 2020 and then annually after that) we will be moving accounts into the account based strategy and could also be removing accounts or moving them to a lower tier.  This could be because the intent signals have dropped to a level that does not support a custom marketing plan, or the sales team is not supporting the marketing plan as needed.  An explanation will always be given if an account is moved to a different tier or removed from our account based strategy altogether.

## Accounts are identified in Salesforce by the GTM strategy field in Salesforce:

**VOLUME**
Default selection for all accounts.

**ACCOUNT CENTRIC**
Indicates that an account is a focus for field marketing and account centric campaigns.

**ACCELERATION**
Accounts that are a current target of the acceleration team outlined here: https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/#acceleration-marketing

**ACCOUNT BASED**
This defines that an account is included in one of the three tiers of our account based strategy.  If an account is identified with this strategy, the additional field of `ABM Tier ` that will be completed as well which will identify which tier the account falls into in respect to our account based strategy.

**ABM Tier**
This field is a sub field of GTM Strategy and will be populated if `Account Based` is chosed in the `GTM Strategy` field.  This field will identify which tier an account is currently in.

## What resources and marketing do each of the tiers receive?
Each campaign for ABM is built similarly to an integrated marketing campaign.

**`Tier 1`** account campaigns are built specific to that account i.e. specifc content and content journey to that account

**`Tier 2`** account campaigns are semi-customized based on firmographic or use case campaign plans.  An example would be a campaign specific to financial service companies that fit our ideal customer profile, or a campaign built around a set of accounts that are exhibiting `HIGH` intent signals and have a use case in common.

**`Tier 3`** programatic campaign that is less resource heavy and a lighter lift to develop and execute than tier 2 or tier 1.  This is more of a "one size fits most" tactic, relying on the account team to customize outreach as needed.

## How we use intent data
We use intent data to help us target the right accounts at the right time.  By knowing when an account is has the highest propensity to buy, we can target accounts with the right resources at the right time.  We utilize Demandbase for intent data, which integrates with Salesforce to serve this information at the account level when the `domain` field is populated.

### Intent fields & definitions

In salesforce, you will see a set of fields at the account level under the Demandbase section.  Below is a list of those fields and their definitions.
**Score** Demandbase uses AI to qualify accounts and give them a score based on their match to our ICP and their engagement on our website, and also offsite intent.  Score are ranked High, Medium, and Low.  Demandbase's AI uses our ideal customer profile (Intent Keywords/Buyer Titles/Customers) to model and score/rank accounts based on:

*  Intent from target accounts

*  Buying committee titles within profile

*  Firmographics of customers in profile (Revenue, industry, geo location, employee size, etc.)

*  Products sold by customers in profile

**Trending onsite engagement** Shows us which accounts have been more engaged and active on our site over the last week compared to the last two months.

**Page views** The number of unique page views within the last thirty days.  We have scrubbed pages such as the careers page etc that do not show an intent to research or purchase GitLab.

**Sessions** Shows the number of unique visits the account has made to our website in the last thirty days.  A session is defined as a unique visit to our site of up to 30 minutes.  If they spend more than 30 minutes on our site this will count as two sessions.  It is generally considered that an account is engaged if they have three or more sessions in the past thirty days.

**Last Seen** The number of days since an account was last on our website.

**Rank** How an account ranks based on our profile within Demandbase

**Intent** This field will show you the top five keywords the account is researching

**Trending offsite intent** Shows if the account has had a recent spike in offsite research (in the last week compared to the last two months), including competitor research

**Change traffic MOM** Percentage of change, either increase or decrease of traffic on our website from the account month over month.

### Additional Demandbase information
There is additional information from Demandbase in Salesforce.

**Title**
Name of the audience or campaign an account is associated with in Demandbase

## Other helpful definitions
**Total addressable Market (TAM)**-
Also called total available market, total addressable market references the revenue opportunity available for a product or service. TAM helps to prioritize business opportunities by serving as a quick metric of the underlying potential of a given opportunity

**Ideal customer profile (ICP)**- 
Ideal customer profile is a description of a company who is the best fit for our solution.  This can include firmographics, environmental and behavioral characteristics.  We use this profile to align our account based marketing efforts

**Target accounts**- 
Accounts that fit our ideal customer profile that we will focus our account base strategy on.  Target accounts are simply accounts that we would like to make customers

**Tiered Accounts**- 
Our account based strategy will include tiering our target accounts based on a number of factors so that we can market appropriately to them.  We consider our ICP, intent signals, and other data when adding accounts to a tier.


